import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Router from './src/Router/Router'

const App = () => {
  return (
    <Router/>
  )
}

export default App

const styles = StyleSheet.create({})