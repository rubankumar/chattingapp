import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Login from '../Screen/Login/Login';
import Chat from '../Screen/Chat/Chat';
import Welcome from '../Screen/Welcome/Welcome';

const stack = createNativeStackNavigator();

const RootStack = () => {
  return (
    <stack.Navigator initialRouteName="Login" screenOptions={{ headerShown: false}}>
      <stack.Screen name="Login" component={Login} />
      <stack.Screen name="Chat" component={Chat} />
      <stack.Screen name="Welcome" component={Welcome} />
    </stack.Navigator>
  );
};

export default RootStack;

const styles = StyleSheet.create({});
