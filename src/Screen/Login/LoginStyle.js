import {StyleSheet} from 'react-native'

export default  Styles = () => StyleSheet.create({

container:{
    flex:1,
    paddingLeft:20,
    paddingTop:20,
    backgroundColor:'#ffffff'
},
Welcome:{
   height:200,
   width:200,
   alignSelf:'center',
},
Top:{
    flex:0.3,
},
top1:{
    flex:0.2,
    marginTop:40
},
input:{
    height:50,
    width:'90%',
    backgroundColor:'#fff',
    borderColor:'#6c63ff',
    borderWidth:1,
    borderRadius:5,
    marginTop:10
},
bottom:{
    flex:0.5,
    alignSelf:'center',
    marginTop:40
},
arrow:{  
    padding:5,
    paddingLeft:17
},
arrowview:{
    backgroundColor:'#6c63ff',
    borderRadius:50,
    height:50,
    width:50,
    justifyContent:'center',
   


},
Alreadybtn:{
    marginTop:20,
    
},
Alreadytxt:{
    color:'#3b95ff'
}




})

