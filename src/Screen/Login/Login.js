import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
  SafeAreaView,
  Image,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Styles from './LoginStyle';
import Icon from 'react-native-vector-icons/FontAwesome';
import database from '@react-native-firebase/database';
import moment from 'moment';
import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const Login = ({navigation}) => {
  const [user, setUser] = useState('');
  const [num, setNum] = useState('');

  const moment = require('moment');
  const time = moment();

  // useEffect(()=>{
  //   DatabaseFn();
  // })

  const DatabaseFn = () => {
    if (user === '' || num == '') {
      Alert.alert('please enter your name');
    } else {
      console.log('show');
      const reference = database()
        .ref()
        .child('user')
        .orderByChild('num')
        .equalTo(num)
        .once('value')
        .then(snapshot => {
          if (snapshot.exists()) {
            let userData = snapshot.val();
            console.log(userData);
            let passData = num;
            let passuser = user;
            navigation.navigate('Welcome', {
              passnum: passData,
              passname: passuser,
            });
          } else {
            console.log('not found');
            database()
              .ref(`/user/${num}`)
              .set({
                name: user,
                unix: time.unix(),
                utc: time.format('YYYY-MM-DD HH:mm:ss'),
                uuid: uuidv4(),
                num: num,
              });
            let passData = num;
            let passuser = user;
            navigation.navigate('Welcome', {
              passnum: passData,
              passname: passuser,
            });
          }
        });
    }
  };

  return (
    <KeyboardAwareScrollView style={Styles().container}>
      <View style={Styles().Top}>
        <Image
          source={require('../../Images/image1.png')}
          style={Styles().Welcome}
        />
      </View>
      <View style={Styles().top1}>
        <TextInput
          style={Styles().input}
          placeholder="Enter your name"
          onChangeText={text => setUser(text)}
        />
        <TextInput
          style={Styles().input}
          placeholder="Enter your number"
          onChangeText={text => setNum(text)}
          maxLength={10}
        />
      </View>
      <View style={Styles().bottom}>
        <TouchableOpacity
          style={Styles().arrowview}
          onPress={() => DatabaseFn()}>
          <Icon
            name="chevron-right"
            size={20}
            color={'#fff'}
            style={Styles().arrow}
          />
        </TouchableOpacity>
      </View>
    </KeyboardAwareScrollView>
  );
};

export default Login;

const styles = StyleSheet.create({});
