import {StyleSheet} from 'react-native'

export default Styles = () => StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#ffffff'
    },
    head:{
        flex:0.1,
        flexDirection:'row',
        backgroundColor:'#3b95ff',
        alignItems:'center',
        justifyContent:'space-around'
    },
    message:{
        fontSize:25,
        color:'#ffffff',
        paddingLeft:30
       
    },
    top:{
        flex:0.9,
    },
    flatcontainer:{
    flexDirection:'row',
    height:80,
    borderBottomColor:'#c5c5c5',
    borderBottomWidth:0.5,
    width:'90%',
    marginLeft:20,
    backgroundColor:'#ffffff'
    },
    img:{
        height:40,
        width:40
    },
    names:{
        color:'#000',
        fontSize:20,
        marginLeft:15
    },
    flatbtn:{
        flexDirection:'row',
        alignItems:'center'
   
    },
    passuser:{
        backgroundColor:'#FF1493',
        color:'#ffffff',
        fontSize:20,
        borderRadius:50,
        height:40,
        width:40,
        textAlign:'center',
        paddingTop:6
       

    }
})