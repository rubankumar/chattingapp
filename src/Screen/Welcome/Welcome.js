import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Styles from './WelcomeStyle';
import Icon from 'react-native-vector-icons/FontAwesome';
import database from '@react-native-firebase/database';

const Welcome = ({navigation, route}) => {
  const [itemsArray, setItemsArray] = React.useState([]);
  const {passnum, passname} = route.params;
  const [Data, setData] = useState([
    {
      id: 1,
      name: 'danny',
      uri: 'https://www.vhv.rs/dpng/d/577-5773494_circle-color-circle-hd-png-download.png',
    },
    {
      id: 2,
      name: 'Ruban',
      uri: 'https://www.vhv.rs/dpng/d/577-5773494_circle-color-circle-hd-png-download.png',
    },
    {
      id: 3,
      name: 'Surya',
      uri: 'https://www.vhv.rs/dpng/d/577-5773494_circle-color-circle-hd-png-download.png',
    },
    {
      id: 4,
      name: 'Mohan',
      uri: 'https://www.vhv.rs/dpng/d/577-5773494_circle-color-circle-hd-png-download.png',
    },
    {
      id: 5,
      name: 'Rahul',
      uri: 'https://www.vhv.rs/dpng/d/577-5773494_circle-color-circle-hd-png-download.png',
    },
  ]);

  React.useEffect(() => {
    database()
      .ref(`/user`)
      .on('value', snapshot => {
        console.log('User data: ', snapshot.val());
        let data = snapshot.val();
        const items = Object.values(data);
        setItemsArray(items);
      });
  }, []);

  let passno = passnum;
  let passuser = passname.substring(0, 2).toUpperCase();
  console.log('name', itemsArray);

  const chatHistoryCreate = item => {
    let numbers = passno.concat(item.num);

    if (item.hasOwnProperty('Chat_ID')) {
      if (item.Chat_ID.hasOwnProperty(passno)) {
        console.log('item.Chat_ID', item.num);
        const dd = database()
          .ref(`/user/${item.num}/Chat_ID/${passno}`)
          .once('value')
          .then(snapshot => {
            console.log('User data: ', snapshot.val());
            let getData = snapshot.val();
            navigation.navigate('Chat', {chatID: getData.user_id});
          });
      } else {
        database()
          .ref(`/user/${passno}/Chat_ID/${item.num}/`)
          .set({
            user_id: numbers,
          })
          .then(() => {
            database()
              .ref(`/user/${item.num}/Chat_ID/${passno}/`)
              .set({
                user_id: numbers,
              })
              .then(() => {
                navigation.navigate('Chat', {chatID: numbers});
              });
          });
      }
    } else {
      database()
        .ref(`/user/${passno}/Chat_ID/${item.num}/`)
        .set({
          user_id: numbers,
        })
        .then(() => {
          database()
            .ref(`/user/${item.num}/Chat_ID/${passno}/`)
            .set({
              user_id: numbers,
            })
            .then(() => {
              navigation.navigate('Chat', {chatID: numbers});
            });
        });
    }
  };

  const renderItem = ({item}) => {
    console.log('num', item.num);

    return (
      <View style={Styles().flatcontainer}>
        <TouchableOpacity
          style={Styles().flatbtn}
          onPress={
            () => chatHistoryCreate(item)
            // navigation.navigate('Chat', {
            //   passnumber: passno,
            //   passsecNo: item.num,
            // })
          }>
          <Text style={Styles().passuser}>{passuser}</Text>
          <Text style={Styles().names}>{item.num}</Text>
        </TouchableOpacity>
      </View>
    );
  };
  return (
    <View style={Styles().container}>
      <View style={Styles().head}>
        <Icon
          name="arrow-left"
          size={25}
          color={'#fff'}
          onPress={() => navigation.navigate('Login')}
        />
        <Text style={Styles().message}>message</Text>
        <Text style={Styles().passuser}>{passuser}</Text>
      </View>
      <View style={Styles().top}>
        <FlatList
          data={itemsArray.filter(fil => fil.num !== passnum)}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
      </View>
    </View>
  );
};

export default Welcome;

const styles = StyleSheet.create({});
