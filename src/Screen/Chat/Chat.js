import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TextInput,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import React, {useState} from 'react';
import Styles from './ChatStyle';
import Icon from 'react-native-vector-icons/FontAwesome';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import database from '@react-native-firebase/database';

const Chat = ({navigation, route}) => {
  const [data, setData] = useState([]);
  const [inputText, setInputText] = useState('');
  const {chatID} = route.params;
  console.log('num1', chatID);

  React.useEffect(() => {
    database()
      .ref(`/chat_history/${chatID}`)
      .on('value', snapshot => {
        let receiverData = snapshot.val();
        if (receiverData !== null) {
          console.log('dd', data);
          const items = Object.values(receiverData);
          setData(items);
        } else {
          console.log('dd', data);
        }
      });
  }, []);

  const Bindup = () => {
    if (inputText !== '') {
      database().ref(`/chat_history/${chatID}`).push({
        text: inputText,
      });
    } else {
      alert('Type Something');
    }
  };

  const renderItem = ({item}) => {
    return (
      <View
        style={[
          Styles().flatcontainer,
          // {alignItems: item.flag == 0 ? 'flex-start' : 'flex-end'},
        ]}>
        <Text
          style={[
            Styles().message,
            {
              color: '#fff',
              backgroundColor: '#3b95ff',
              textAlign: 'right',
            },
          ]}>
          {item.text}
        </Text>
      </View>
    );
  };

  return (
    <View style={Styles().container}>
      <View style={Styles().head}>
        <Icon name="arrow-left" size={25} color={'#fff'} />
        <Text style={Styles().chat}>Chats</Text>
      </View>
      <View style={Styles().top}>
        <FlatList
          data={data}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
      </View>
      <View style={Styles().bottom}>
        <TextInput
          style={Styles().textinp}
          placeholder="Type a message"
          value={inputText}
          onChangeText={text => setInputText(text)}
        />
        <TouchableOpacity onPress={Bindup}>
          <Icon
            name="arrow-right"
            color={'#000'}
            size={30}
            style={{marginLeft: 10}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Chat;

const styles = StyleSheet.create({});
