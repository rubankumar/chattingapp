import {StyleSheet} from 'react-native';

export default Styles = () => StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#ffffff'
    },
    head:{
        flex:0.1,
        backgroundColor:'#3b95ff',
        flexDirection:'row',
        alignItems:'center',
        paddingLeft:20

    },
    chat:{
        fontSize:25,
        color:'#ffffff',
        paddingLeft:30,

       
    },
    top:{
        flex:0.7,
    },
    flatcontainer:{
        marginTop:10,
        height:80,
        borderRadius:10,
        justifyContent:'center',
        alignItems:'center'
    },
   message:{
       fontSize:20,
       width:150,
       height:50,
       borderRadius:5
   },
   bottom:{
      flexDirection:'row',
      justifyContent:'center',
      alignItems:'flex-start',
      flex:0.2,
      marginTop:40,
      alignItems:'center'
     
   },
   textinp:{
       width:'80%',
       height:40,
       borderColor:'#000',
       borderWidth:1,
       borderRadius:5,
    
       

   }
})